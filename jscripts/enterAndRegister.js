$(document).ready(function() {
	$("#hiddenBlock").keydown(function(eventObject) {
		if (eventObject.which == 13) {
			if ($("#hiddenBlock").attr("act") == "reg") {
				sendFormForRegister();
			} else {
				sendFormForEnter();
			}
		}
	});
});
	var enterUsers = function() {
		if ($("#hiddenBlock").attr("act") == "") {
			$("#enter_block_b1").slideToggle(210);
			$("#hiddenBlock").attr("act", "enter");
			$("#hiddenBlock div[name='currentAction']").html("Форма входа");
			$("#hiddenBlock input[name='submit']").attr("onclick", "sendFormForEnter()");
			setTimeout(function() {
				$("#hiddenBlock").slideToggle(200);
				$("#hiddenBlock input[name='login']").focus();
			}, 0);
		} else {
			$("#enter_block_b2").html("Регистрация");
			$("#enter_block_b2").attr("onclick", "registerUsers()");
			$("#hiddenBlock").attr("act", "enter");
			$("#hiddenBlock div[name='currentAction']").html("Форма входа");
			$("#hiddenBlock input[name='submit']").attr("onclick", "sendFormForEnter()");
			$("#hiddenBlock input[name='login']").focus();
			$("#errorMess").html("");
		}
	}
	var registerUsers = function() {
		if ($("#hiddenBlock").attr("act") == "") {
			$("#enter_block_b1").slideToggle(210);
			$("#enter_block_b2").html("Вход");
			$("#enter_block_b2").attr("onclick", "enterUsers()");
			$("#hiddenBlock").attr("act", "reg");
			$("#hiddenBlock div[name='currentAction']").html("Форма регистрации");
			$("#hiddenBlock input[name='submit']").attr("onclick", "sendFormForRegister()");
			setTimeout(function() {
				$("#hiddenBlock").slideToggle(200);
				$("#hiddenBlock input[name='login']").focus();
			}, 0);
		} else {
			$("#enter_block_b2").html("Вход");
			$("#enter_block_b2").attr("onclick", "enterUsers()");
			$("#hiddenBlock").attr("act", "reg");
			$("#hiddenBlock div[name='currentAction']").html("Форма регистрации");
			$("#hiddenBlock input[name='submit']").attr("onclick", "sendFormForRegister()");
			$("#hiddenBlock input[name='login']").focus();
			$("#errorMess").html("");
		}
	}

	var sendFormForRegister = function() {
		var login = $("#hiddenBlock input[name='login']").val();
		var pass = $("#hiddenBlock input[name='pass']").val();
		$.ajax({
			type: "POST",
			url: 'ajax/registration.php',
			data: {
				login: login,
				pass: pass
			},
			success: function(answer) {
				if (answer == 'true') {
					setTimeout(function() {window.location.reload();}, 0);
				} else {
					$("#errorMess").html(getErrorMessageFromCode(answer));
				}
			}
		});
	}
	var sendFormForEnter = function() {
		var login = $("#hiddenBlock input[name='login']").val();
		var pass = $("#hiddenBlock input[name='pass']").val();
		$.ajax({
			type: "POST",
			url: 'ajax/enter.php',
			data: {
				login: login,
				pass: pass
			},
			success: function(answer) {
				if (answer == 'true') {
					setTimeout(function() {window.location.reload();}, 0);
				} else {
					$("#errorMess").html(getErrorMessageFromCode(answer));
				}
			}
		});
	}