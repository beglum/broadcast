var findNewMessages = function() {
	$.ajax({
		type: "POST",
		data: "lastMessage="+$("#h_lastmessage").val(),
		url: "ajax/chat/inspectionLastMessage.php",
		success: function(answer) {
			if (answer != "null") {
				var data = JSON.parse(answer);
				$("#h_lastmessage").val(data.lastMessage);
				$.each(data.newMessages, function() {
					var newMessage = getMess(this[0], this[1], this[2], this[3]);
					$("#bigcanvas").append(newMessage);

					var oldScroll = ($("#bigcanvas").height() + 20) - $("#messages_box").height() - $("#messages_box").scrollTop();
					if (oldScroll < 200) {
						$("#messages_box").scrollTop($("#bigcanvas").height()+20 - $("#messages_box").height());
					}
				});
			}
			// while ($("#bigcanvas").height() > 1500) {
			// 	$("#bigcanvas .message_box:first-child").remove();
			// }
		}
	});
	setTimeout(arguments.callee, 1000);
}
setTimeout(findNewMessages, 1000);
$(document).ready(function() {
	$("#input_message_box input").focus();
	$("#input_message_box input").keydown(function(eventObject) {
		if (eventObject.which == 13) {
			var name = $("#myName input").val();
			var newMessage = $("#input_message_box input").val();
			
			if (newMessage == "") {
				return false;
			}

			$.ajax({
				type: "POST",
				url: 'ajax/chat/sendMessage.php',
				data: {
					message: newMessage
				},
				success: function(answer) {
					var oldMessages = $("#messages_box").html();
					newMessage = answer;
					$("#messages_box").html(oldMessages + newMessage);
					$("#input_message_box input").val("");
				}
			});
		}
	});
});