var getErrorMessageFromCode = function (code) {
	// Возвращает описание кода ошибки
	var answer = "";
	switch (code) {
		case "e000_001":
			answer = "Одно из полей пустое";
		break;
		case "e000_002":
			answer = "Такой логин уже используется";
		break;
		case "e000_003":
			answer = "Нет совпадения логина и пароля";
		break;
	}
	return answer;
}

// Функция, которая обрабатывает сообщение перед выводом
var getMess = function (avatar, name, mess, mid) {
	var newMessage = "<div class='message_box'><div class='message_box_avatar'>";
	var nameblock = "";
	var message = "";
	if (avatar != "/") {
		newMessage += "<img";
		if (mid == false) newMessage += " style='margin-left: -5px;'";
		newMessage += " src='/images/avatars/" + avatar + "' />";
		nameblock = "<div class='message_box_name'>" + name;
		if (mid != false) {
			nameblock += "<div>#"+mid+"</div>";
		}
		nameblock += "</div>";
		message = mess;

		// Определяем заголовки ответов
		var doWhile = true;
		var last_char = 0;
		while (doWhile) {
			// * Мы ищем первое вхождение знака заголовка (#);
			// * Если заголовки в сообщении не найдены - выходим из скрипта;
			var first_char = message.indexOf("#", last_char);
			if (first_char == -1) {
				doWhile = false;
			} else {
				// * Далее ищем окончаение заголовка (,);
				// * Если окончание не найдено - выходим из скрипта;
				var comma = message.indexOf(",", first_char);
				if (comma == -1) {
					doWhile = false;
				} else {
					// * Ищем первое вхождение пробела после начала заголовка;
					// * Если такой не найден - записываем длину всего сообщения;
					var space = message.indexOf(" ", first_char);
					if (space == -1) {
						space = message.length;
					}
					// * Показываем сколько нужно удалить знаков из оригинального сообщения
					// * после заголовка, так как мы должны узнать есть пробел или нет после запятой;
					var space_delete = 2; // Количество удаляемых символов после заголовка

					// * Если первое вхождение пробела было раньше запятой - значит это не заголовок, выходим;
					if (comma > space) {
						doWhile = false;
					} else {
						// Уменьшаем кол-во удаляемых символов, так как пробела рядом нет;
						if (space - comma > 1) space_delete--;
						var sub_str = message.slice(first_char+1, comma);
						//alert(sub_str);
						// * Если при преобразовании строки в числа вышло NaN - значит в ответе зачесалась ошибка, выходим;
						if (isNaN(sub_str)) {
							doWhile = false;
						} else {
							// * В остальном случае начинаем формировать блок заголовка и внедрять в сообщение;
							sub_str = "<tt class='answer_code'>#"+sub_str+"</tt>,&nbsp;";
							message = message.slice(0, first_char) + sub_str + message.slice(comma+space_delete);
							last_char = comma+36;
						}
					}
				}
			}
		}

	} else {
		message = "<i style='font-size: 14px; display: block; padding-top: 10px;'>--- " + this[2] + "</i>";
	}
	newMessage += "</div><div style='float: left; width: 90%;'>"+nameblock+"<div class='message_box_message'>" + message + "</div></div><div style='clear: both;'></div></div>";
	return newMessage;
}
// Функция закрытия редактора имени
var closeEditorName = function($self) {
	var newName = $("#nameEdit input").val().trim();
	if ($self == "true" && newName != $("#nameEdit").attr("oldname")) {
		$.ajax({
			type: "POST",
			data: "newName="+newName,
			url: "ajax/nameUpdate.php"
		});
	}
	$("#myName div[name='name']").html(newName);
	setTimeout(function() {
		$("#myName div[name='name']").attr("onclick","editName()");
	}, 0);
}
// Функция запуска редактора имени
var editName = function () {
	var name = $("#myName div[name='name']").html();
	if ($("#avatarPanel").css("display") != "none") {
		closeAvatarsPanel();
	}
	$("#myName div[name='name']").attr("onclick","");
	$("#myName div[name='name']").html("<div id='nameEdit' oldname='"+$.trim(name)+"'><input maxlength='12' style='width: 100%; height: 100%;' type='text' value='"+$.trim(name)+"'></div><div id='acceptNewName' onclick='closeEditorName(\"true\")'>СМЕНИТЬ</div>");
	$("#nameEdit input").focus();
	$("#nameEdit input").setSelectionRange($("#nameEdit input").val().length,$("#nameEdit input").val().length);
}
// Функция запуска выбора аватара
var clickOnAvatar = function(){
	if ($("#myName div[name='name']").attr("onclick") == "") {
		closeEditorName("false");
	}
	$("#myAvatar div:first-of-type img").attr("onclick", "closeAvatarsPanel()");
	$("#avatarPanel").toggle();
	$("#opacityscreen").toggle();
	$.ajax({
		type: "POST",
		data: {"gethash": true},
		url: "ajax/getMyAvatars.php",
		success: function(hash) {
			if (hash != $("#avatarPanel").attr("hash")) {
				$("#avatarPanel").attr("hash", hash);
				$.ajax({
					type: "POST",
					url: "ajax/getMyAvatars.php",
					success: function(answer) {
						var avatarsList = $.parseJSON(answer);
						$("#avatarsList").html("<div class='avatarOnAvatarPanel' onclick=\"uploadNewAvatar()\"></div>");
						for (var i = 1; i <= avatarsList.length; i++) {
							var add = "<div class='avatarOnAvatarPanel'><img class='imgOnAvatarList' width='64px' height='64px' src='/images/avatars/"+avatarsList[i-1][0]+"' name='"+avatarsList[i-1][1]+"' mid='"+avatarsList[i-1][2]+"'>"
							if (i != avatarsList.length) add += "<div class='deleteAvatar'></div>";
							add += "</div>";
							$("#avatarsList").append(add);
						}
					}
				});
			}
		}
	});
}
// Фукнция закрытия выбора аватара
var closeAvatarsPanel = function() {
	$("#avatarPanel").toggle();
	$("#myAvatar div:first-of-type img").attr("onclick", "clickOnAvatar()");
	$("#opacityscreen").toggle();
}
// Функция загрузки нового аватара
var uploadNewAvatar = function() {
	setTimeout(function() {
		$("#uploadNewAvatar input[type='file']").trigger('click');
	}, 0);
}

$(document).ready(function() {
	document.getElementById('avatarPanel').onwheel = function(e) {
		var move = e.deltaY || e.detail || e.wheelDelta;
		move = move / Math.abs(move) * 22;
		document.getElementById('avatarsList').scrollLeft += move;
	};
	$("#myName").keydown(function(eventObject) {
		if (eventObject.which == 13) {
			closeEditorName("true");
			return false;
		}
	});
	var bgChat = 238;
	var changeBGChat = function() {
		if (bgChat > 238) bgChat = 238;
		if (bgChat < 188) bgChat = 188;
		$("#block_history_main").css("background-color", "rgb("+bgChat+","+bgChat+","+bgChat+")");
		$("#input_message_box input").css("background-color", "rgb("+bgChat+","+bgChat+","+bgChat+")");
	}
	$(document).keydown(function(eventObject) {
		if (eventObject.which == 107) {
			bgChat += 10;
			changeBGChat();
		}
		if (eventObject.which == 109) {
			bgChat -= 10;
			changeBGChat();
		}
	});
	// Триггер нажатия на изображение в панели выбора аватара
	$("body").on("click",".avatarOnAvatarPanel img" , function(event) {
		var code = event.target.name;
		var mid = $(event.target).attr("mid");
		$.ajax({
			type: "POST",
			url: "ajax/changeMyAvatar.php",
			data: {
				code: code,
				mid: mid
			},
			success: function(answer) {
				if (answer == "1") {
					$("#changeAvatar img").attr("src", event.target.src);
					closeAvatarsPanel();
					$("#changeAvatar img").attr("onclick", "clickOnAvatar()");
				}
			}
		});
	});
	// Триггер нажатия на кнопку удаления аватара
	$("body").on("click",".avatarOnAvatarPanel > .deleteAvatar" , function(event) {
		var code = event.target.previousSibling.name;
		var mid = $(event.target.previousSibling).attr("mid");
		$.ajax({
			type: "POST",
			url: "ajax/deleteMyAvatar.php",
			data: {
				code: code,
				mid: mid
			},
			success: function(answer) {
				if (answer != "true" && answer != "false") {
					$("#changeAvatar > img").attr("src", "images/avatars/"+answer);
					$(event.target.parentNode).remove();
				} else if (answer == "true") {
					$(event.target.parentNode).remove();
				}
			}
		});
	});
	// Триггер нажатия на имя пользователя в чате
	$("body").on("click",".message_box_name > div" , function(event) {
		var oldMess = $("#input_message_box > input").val();
		var foundSubStr = oldMess.indexOf($(this).html());
		if (foundSubStr == -1) {
			$("#input_message_box > input").val($(this).html()+", "+oldMess);
		} else {
			var newMess = oldMess.slice(0,foundSubStr)+oldMess.slice(foundSubStr+($(this).html().length+2));
			$("#input_message_box > input").val(newMess);
		}
		$("#input_message_box > input").focus();
	});
	var indext = 0;
	$("body").on("mouseenter", ".answer_code", function() {
		if ($(this).attr("over") != "on") {
			$(this).attr("over", "on");
			var add_message = "";
			var obj = $(this);
			$.ajax({
				type: "POST",
				data: {"mid": $(this).html().slice(1)},
				url: "ajax/chat/findmess.php",
				success: function(answer) {
					if (answer != "null" || answer != "") {
						var data = JSON.parse(answer);
						var mess = getMess(data[0], data[1], data[2], false);
					} else {
						var mess = "ERROGE";
					}
					indext++;
					$(obj).append("<div style='position: fixed; z-index: 4;' name='block'>"+mess+"</div>");
					$("div[name='block']", obj).offset(function(i, coord){
						var newCoord = {};
						newCoord.left = coord.left;
						newCoord.top = $(obj).offset().top + 22;
						return newCoord;
					});
				}
			});
		}
	});
	var closeBoard = function(obj) {
		if ($(obj).attr("over") == "on") {
			$("div", obj).remove();
			indext--;
			$(obj).attr("over", "off");
		}	
	}
	$("body").on("mouseleave", ".answer_code", function() {
		var obj = $(this);
		setTimeout(function() {
			if ($(obj).is(":hover")) {
				return;
			}
			closeBoard(obj);
		}, 1500);
	});

	$("#uploadNewAvatar input[type='file']").change(function() {
		$("#uploadNewAvatar input[type='submit']").trigger('click');
	});
	$("#opacityscreen").click(function() {
		$("#avatarPanel").click(function() {
			return;
		});
		closeAvatarsPanel();
	});
});