<?php
$path = "images/1.jpg";
$imgdata = getimagesize($path);
list($width, $height) = $imgdata;

switch ($imgdata['mime']) {
	case "image/png":
		$oldImage = imagecreatefrompng($path);
	break;
	case "image/jpeg":
		$oldImage = imagecreatefromjpeg($path);
	break;
}

if ($width > $height) {
	$min = $height;
	$indentx = $width/2-$height/2;
	$indenty = 0;
} else {
	$min = $width;
	$indentx = 0;
	$indenty = $height/2-$width/2;
}
$newImage = imagecreatetruecolor(64, 64);
$bgcolor = imagecolorallocate($newImage, 255, 255, 255);
imagefilledrectangle($newImage, 0, 0, 64, 64, $bgcolor);
imagecopyresampled($newImage, $oldImage, 0, 0, $indentx, $indenty, 64, 64, $min, $min);
imagejpeg($newImage, "images/3.jpg", 100);
?>