<?php
	include 'config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Эфир</title>
	<link href="style/default_style.css" rel="stylesheet">
	<script src="jscripts/jquery.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=cyrillic" rel="stylesheet">
	<script src="jscripts/baseFunctions.js"></script>
	<script src="jscripts/newMessages.js"></script>
</head>
<body>
	<div id='opacityscreen'></div>
	<div id='darkscreen'></div>
	<div id="main_block">
		<div class="side_block">
			<?php
				include('modules/left_side_block.php');
			?>
		</div>
		<div id="block_history_main">
			<div id="messages_box">
				<div id="bigcanvas"></div>
			</div>
			<div id="input_message_box">
				<input type="text" name="text" maxlength="253" <?php if ($logon == false) { echo "disabled value='Для отправки сообщений необходимо авторизоваться'"; } ?> autocomplete="off">
			</div>
		</div>
	</div>
	<div id="debug_box"></div>
</body>
</html>