<?php
	include '../config.php';

	$arrayAvatars = array();

	// Цикл для начала запрашивает у сервера аватарки, владельцем которых является пользователь 
	// с данным ID. После он вычитает из счетчика ID пользователя, чтобы запросить у сервера дефолтные (владелец: 0)
	// аватарки.

	for ($i = $_SESSION['id']; $i >= 0; $i -= $_SESSION['id']) {
		$result = $mysqli->query("SELECT `id`,`name`,`code` FROM `avatars` WHERE `owner`='$i'");
		for ($row_no = $result->num_rows - 1; $row_no >= 0; $row_no--) {
			$result->data_seek($row_no);
			$row = $result->fetch_assoc();
			$image = array();
			$image[] = $i.'&#047;'.$row['name'];
			$image[] = $row['code'];
			$image[] = $row['id'];
			$arrayAvatars[] = $image;
			unset($image);
		}
	}
	$JSONresult = json_encode($arrayAvatars);
	if ($_POST['gethash'] == true) {
		exit (md5($JSONresult));
	} else {
		exit ($JSONresult);
	}
?>