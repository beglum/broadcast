<?php
	include '../config.php';
	$login = strip_tags(trim($_POST['login']));
	$pass = strip_tags(trim($_POST['pass']));
	if ($login == "" or $pass == "") {
		// Одно из полей оказалось пустым
		exit("e000_001");
	}
	$result = $mysqli->query("SELECT * FROM users WHERE login='".$login."'");
	if ($result->num_rows == 0) {
		// Нет совпадения логина и пароля
		exit("e000_003");
	}
	$result = $result->fetch_assoc();
	if ($result['pass'] != $pass) {
		// Нет совпадения логина и пароля
		exit("e000_003");
	}
	$_SESSION['login'] = $login;
	$_SESSION['pass'] = $result['pass'];
	$_SESSION['name'] = $result['name'];
	$_SESSION['id'] = $result['id'];
	$getAvatar = $mysqli->query ("SELECT * FROM avatars WHERE id='".$result['avatar']."'");
	$getAvatar = $getAvatar->fetch_assoc();
	$_SESSION['avatar'] = $getAvatar['owner']."/".$getAvatar['name'];
	exit("true");
?>