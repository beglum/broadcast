<?php
	include '../config.php';

	function generateName($length) {
		$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789-_';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string; 
	}

	$uploaddir = '../images/avatars/'.$_SESSION['id']."/";
	if ($_FILES['image']['size'] == 0) {
		exit("Файл не был загружен");
	} else if ($_FILES['image']['size'] > 3000000) {
		exit("С-семпай! Слишком большой..");
	}
	if ($_FILES['image']['type'] != "image/jpeg" AND $_FILES['image']['type'] != "image/png" AND $_FILES['image']['type'] != "image/bmp") {
		exit("Неправильное расширение файла");
	}
	if (!file_exists($uploaddir)) {
		if (!mkdir($uploaddir)) {
			exit("Не могу создать дирректорию");
		}
	}
	$type = substr($_FILES['image']['name'], strpos($_FILES['image']['name'], "."));
	$newName = generateName(10).$type;

	if (move_uploaded_file($_FILES['image']['tmp_name'], $uploaddir.$newName)) {
		// Конвертация изображения
			$oldimage = $uploaddir.$newName;
			$imgdata = getimagesize($oldimage);
			list($width, $height) = $imgdata;
			switch ($_FILES['image']['type']) {
				case "image/png":
					$oldimage = imagecreatefrompng($oldimage);
				break;
				case "image/jpeg":
					$oldimage = imagecreatefromjpeg($oldimage);
				break;
				case "image/bmp":
					$oldimage = imagecreatefrombmp($oldimage);
				break;
			}

			if ($width > $height) {
				$min = $height;
				$indentx = $width/2-$height/2;
				$indenty = 0;
			} else {
				$min = $width;
				$indentx = 0;
				$indenty = $height/2-$width/2;
			}

			$newimage = imagecreatetruecolor(64, 64);
			$bgcolor = imagecolorallocate($newimage, 255, 255, 255);
			imagefilledrectangle($newimage, 0, 0, 64, 64, $bgcolor);
			imagecopyresampled($newimage, $oldimage, 0, 0, $indentx, $indenty, 64, 64, $min, $min);
			imagejpeg($newimage, $uploaddir.$newName, 100);
		// Конец конвертация изображения	
		$mysqli->query("INSERT INTO `avatars` (`owner`,`name`,`code`) VALUES ('".$_SESSION['id']."','".$newName."','".generateName(6)."')");
	} else {
		exit("Не смог загрузить изображение");
	}
	$mysqli->query("UPDATE `users` SET `avatar`='".$mysqli->insert_id."' WHERE `id`='".$_SESSION['id']."'");
	$_SESSION['avatar'] = $_SESSION['id']."/".$newName;
	include '../scripts/updateMessage.php';
	sendUpdate("002");
	header("Location: /");
?>