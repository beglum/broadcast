<?php
	if ($logon == true) {
		?>
			<div id="myAvatar">
				<div id="changeAvatar"><img width='64px' height='64px' src="/images/avatars/<? echo $_SESSION['avatar']; ?>" onclick="clickOnAvatar()" />
				</div>
				<div id='avatarPanel'>
					<div id='uploadNewAvatar'>
						<form enctype="multipart/form-data" action="ajax/sendNewAvatar.php" method="POST">
							<input type='hidden' name="MAX_FILE_SIZE" value="3000000">
							<input type="file" name="image">
							<input type="submit" value="ОТПРАВИТЬ">
						</form>
					</div>
					<div id='avatarsList'></div>
				</div>
			</div>
			<div id="myName" class="leftsideBut">
				<div name="name" onclick="editName()">
					<?php
						echo $_SESSION['name'];
					?>
				</div>
			</div>
			<div class="leftsideBut" id="exitBut">
					<a href="TESTexit.php">Выходец</a>
			</div>
		<?php
	} else {
		?>
			<script src="jscripts/enterAndRegister.js"></script>
			<br><br>
			<div id="enter_block">
				<a onclick="enterUsers()" onmousedown="return false" onselectstart="return false" id="enter_block_b1">Вход</a>
				<div style="display: none;" id="hiddenBlock" act="">
					<div name="currentAction" align="center"></div>
					<div id="errorMess" align='center' style="color: #ba6060; font-weight: bold; font-size: 14px;"></div>
					<input name="login" type="text" placeholder="Логин" autocomplete="off">
					<input name="pass" type="password" placeholder="Пароль" autocomplete="off">
					<input type="button" name="submit" onclick="" value="ОТПРАВИТЬ">
				</div>
				<a onclick="registerUsers()" onmousedown="return false" onselectstart="return false" id="enter_block_b2">Регистрация</a>
			</div>
		<?php
	}

?>
<input id="h_lastmessage" type="text" value="0" hidden="">